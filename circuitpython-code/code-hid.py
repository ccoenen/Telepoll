# Treat this board as a M0 "trinket", express won't work despite the flash chip.

import microcontroller
import busio
import pulseio
import time
from adafruit_hid.keyboard import Keyboard
from adafruit_hid.keycode import Keycode
from adafruit_hid.consumer_control import ConsumerControl
from adafruit_hid.consumer_control_code import ConsumerControlCode

from digitalio import DigitalInOut, Direction, Pull

sw1 = DigitalInOut(microcontroller.pin.PA27)
sw1.pull = Pull.UP
sw2 = DigitalInOut(microcontroller.pin.PA01)
sw2.pull = Pull.UP
sw3 = DigitalInOut(microcontroller.pin.PA16)
sw3.pull = Pull.UP
sw4 = DigitalInOut(microcontroller.pin.PA03)
sw4.pull = Pull.UP

sw1_led = DigitalInOut(microcontroller.pin.PA22)
sw1_led.direction = Direction.OUTPUT
sw2_led = DigitalInOut(microcontroller.pin.PA00)
sw2_led.direction = Direction.OUTPUT
sw3_led = DigitalInOut(microcontroller.pin.PA17)
sw3_led.direction = Direction.OUTPUT
sw4_led = DigitalInOut(microcontroller.pin.PA04)
sw4_led.direction = Direction.OUTPUT

# PWM examples
#sw2_led = pulseio.PWMOut(microcontroller.pin.PA22, frequency=5000, duty_cycle=0)
#sw2_led.duty_cycle = int(i * 65535 / 100)  # Up
#sw3_led = pulseio.PWMOut(microcontroller.pin.PA11,  frequency=5000, duty_cycle=0)
#sw4_led = pulseio.PWMOut(microcontroller.pin.PA21,  frequency=5000, duty_cycle=0)

# LIS3DH
# import adafruit_lis3dh
#shared_spi = busio.SPI(microcontroller.pin.PA15, microcontroller.pin.PA08, microcontroller.pin.PA09)
#led_spi = busio.SPI(microcontroller.pin.PA19, microcontroller.pin.PA18)
# bitbangio is not part of circuitpython for m0 basic boards :-/ pins don't work with busio.

# Flash
# Pins work in busio! not tested further.
# flash_spi = busio.SPI(microcontroller.pin.PA09, microcontroller.pin.PA08, microcontroller.pin.PA14)

# APA102 Ring
# https://learn.adafruit.com/adafruit-feather-m0-express-designed-for-circuit-python-circuitpython/circuitpython-dotstar
# sadly, this does not work on mine. Clock is constantly 0V.
#import adafruit_dotstar
#num_pixels = 8
#pixels = adafruit_dotstar.DotStar(microcontroller.pin.PB11, microcontroller.pin.PB10, num_pixels, brightness=0.4, auto_write=False,baudrate=10000)
#RED = (255, 0, 0)
#GREEN = (0, 255, 0)
#pixels.fill(RED)
#pixels.show()

# Emulating a keyboard
time.sleep(1)
keyboard = Keyboard()
consumer_control = ConsumerControl()

while True:
    """
    for i in range(100):
        sw1_led.value = False
        sw2_led.value = False
        sw3_led.value = False
        sw4_led.value = False
        if i < 25:
            sw1_led.value = True
        elif i < 50:
            sw2_led.value = True
        elif i < 75:
            sw3_led.value = True
        else:
            sw4_led.value = True
        time.sleep(0.02)
    print("test")
    """

    #sw1_led.value = not sw1.value
    sw2_led.value = not sw2.value
    #sw3_led.value = not sw3.value
    sw4_led.value = not sw4.value

    #if not sw1.value:
    #    consumer_control.send(ConsumerControlCode.PLAY_PAUSE)
    #    while not sw1.value:
    #        pass

    #if not sw3.value:
    #    consumer_control.send(ConsumerControlCode.SCAN_PREVIOUS_TRACK)
    #    while not sw3.value:
    #        pass

    if not sw4.value:
        consumer_control.send(ConsumerControlCode.SCAN_NEXT_TRACK)
        while not sw4.value:
            pass
