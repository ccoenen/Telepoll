#include "shared-bindings/board/__init__.h"

STATIC const mp_rom_map_elem_t board_global_dict_table[] = {
    { MP_ROM_QSTR(MP_QSTR_SW2_LED),  MP_ROM_PTR(&pin_PA00) },
    { MP_ROM_QSTR(MP_QSTR_SW2),  MP_ROM_PTR(&pin_PA01) },
    { MP_ROM_QSTR(MP_QSTR_BAT_SENSE),  MP_ROM_PTR(&pin_PA02) },
    { MP_ROM_QSTR(MP_QSTR_SW4),  MP_ROM_PTR(&pin_PA03) },
    { MP_ROM_QSTR(MP_QSTR_SW4_LED),  MP_ROM_PTR(&pin_PA04) },
    { MP_ROM_QSTR(MP_QSTR_LIS_INT1),  MP_ROM_PTR(&pin_PA05) },
    { MP_ROM_QSTR(MP_QSTR_NRF_CE),  MP_ROM_PTR(&pin_PA06) },
    { MP_ROM_QSTR(MP_QSTR_NRF_CSN),  MP_ROM_PTR(&pin_PA07) },

    { MP_ROM_QSTR(MP_QSTR_LIS_CSN),  MP_ROM_PTR(&pin_PA08) },
    { MP_ROM_QSTR(MP_QSTR_CIPO),  MP_ROM_PTR(&pin_PA09) },
    { MP_ROM_QSTR(MP_QSTR_TX),  MP_ROM_PTR(&pin_PA10) },
    { MP_ROM_QSTR(MP_QSTR_RX),  MP_ROM_PTR(&pin_PA11) },
    { MP_ROM_QSTR(MP_QSTR_COPI),  MP_ROM_PTR(&pin_PA14) },
    { MP_ROM_QSTR(MP_QSTR_SCK),  MP_ROM_PTR(&pin_PA15) },

    { MP_ROM_QSTR(MP_QSTR_SW3),  MP_ROM_PTR(&pin_PA16) },
    { MP_ROM_QSTR(MP_QSTR_SW3_LED),  MP_ROM_PTR(&pin_PA17) },
    { MP_ROM_QSTR(MP_QSTR_LED_DATA),  MP_ROM_PTR(&pin_PA18) },
    { MP_ROM_QSTR(MP_QSTR_LED_CLOCK),  MP_ROM_PTR(&pin_PA19) },
    { MP_ROM_QSTR(MP_QSTR_SW1_LED),  MP_ROM_PTR(&pin_PA22) },
    { MP_ROM_QSTR(MP_QSTR_BAT_CHARGE),  MP_ROM_PTR(&pin_PA23) },

    { MP_ROM_QSTR(MP_QSTR_SW1),  MP_ROM_PTR(&pin_PA27) },
    { MP_ROM_QSTR(MP_QSTR_NRF_INT),  MP_ROM_PTR(&pin_PA28) },
};
MP_DEFINE_CONST_DICT(board_module_globals, board_global_dict_table);
