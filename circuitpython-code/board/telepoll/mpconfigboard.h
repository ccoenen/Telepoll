// https://www.hackster.io/wallarug/circuitpython-creating-custom-boards-8e979e

#define MICROPY_HW_BOARD_NAME "Telepoll v2"
#define MICROPY_HW_MCU_NAME "samd21e18"

// if we use these, they are not available for our code, apparently.
// #define MICROPY_HW_LED_STATUS   (&pin_PA22)
// #define MICROPY_HW_LED_TX   	(&pin_PA00)
// #define MICROPY_HW_LED_RX   	(&pin_PA17)

#define MICROPY_HW_APA102_MOSI   (&pin_PA18)
#define MICROPY_HW_APA102_SCK    (&pin_PA19)

#define MICROPY_PORT_A        (0)
#define MICROPY_PORT_B        (0)
#define MICROPY_PORT_C        (0)

#define CALIBRATE_CRYSTALLESS 1
#define BOARD_HAS_CRYSTAL 0

#define DEFAULT_SPI_BUS_SCK (&pin_PA15)
#define DEFAULT_SPI_BUS_MOSI (&pin_PA14)
#define DEFAULT_SPI_BUS_MISO (&pin_PA09)

#define DEFAULT_UART_BUS_RX (&pin_PA10)
#define DEFAULT_UART_BUS_TX (&pin_PA11)

#define IGNORE_PIN_PA12     1
#define IGNORE_PIN_PA13     1
#define IGNORE_PIN_PA20     1
#define IGNORE_PIN_PA21     1

// USB is always used.
#define IGNORE_PIN_PA24     1
#define IGNORE_PIN_PA25     1

#define IGNORE_PIN_PB01     1
#define IGNORE_PIN_PB02     1
#define IGNORE_PIN_PB03     1
#define IGNORE_PIN_PB04     1
#define IGNORE_PIN_PB05     1
#define IGNORE_PIN_PB06     1
#define IGNORE_PIN_PB07     1
#define IGNORE_PIN_PB08     1
#define IGNORE_PIN_PB09     1
#define IGNORE_PIN_PB10     1
#define IGNORE_PIN_PB11     1
#define IGNORE_PIN_PB12     1
#define IGNORE_PIN_PB13     1
#define IGNORE_PIN_PB14     1
#define IGNORE_PIN_PB15     1
#define IGNORE_PIN_PB16     1
#define IGNORE_PIN_PB17     1
#define IGNORE_PIN_PB22     1
#define IGNORE_PIN_PB23     1
#define IGNORE_PIN_PB30     1
#define IGNORE_PIN_PB31     1
