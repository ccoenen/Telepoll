#define FASTLED_INTERNAL
#define FASTLED_FORCE_SOFTWARE_SPI
#include <FastLED.h>

// defining all port numbers for later use
// port number: see variants.cpp
// https://github.com/arduino/ArduinoCore-samd/blob/master/variants/arduino_zero/variant.cpp
// MX Switches
const uint8_t SW1 = 35; // PA16, D11 on Feather
const uint8_t SW2 = 30; // PB22, not on Feather
const uint8_t SW3 = 1;  // PA10, D0 / TX on Feather
const uint8_t SW4 = 25; // PB03, not on Feather

// LEDs belonging to the MX Switches
const uint8_t SW1_LED = 13; // PA17, D13 on Feather
const uint8_t SW2_LED = 20; // PA22, SDA on Feather
const uint8_t SW3_LED = 0;  // PA11, RX on Feather
const uint8_t SW4_LED = 39; // PA21, not on Feather (D7, used by SD-Card on Adalogger)

// NRF24L01 Radio Module
const uint8_t NRF_SCK = 17; // PA04, D17 or A3 on Feather
const uint8_t NRF_MOSI = 18; // PA05, D18 or A4 on Feather
// const uint8_t NRF_MISO = 8; // PA06, not on Feather (LED #8 on Adalogger)
const uint8_t NRF_MISO = 21; // D21/SCL, borrowed from BAT_CHARGE to be able to develop something.
const uint8_t NRF_IRQ = 9; // PA07, D9 or A7 on Feather
const uint8_t NRF_CE = 15; // PB08, D15 or A01 on Feather
const uint8_t NRF_CSN = 16; // PB09, D16 or A02 on Feather

// Shitty Addon 1.69 Port
const uint8_t SAO_IO1 = 17; // PA04, D17 or A3 on Feather
const uint8_t SAO_IO2 = 18; // PA05, D18 or A4 on Feather
const uint8_t SAO_SCL = 15; // PB08, D15 or A01 on Feather
const uint8_t SAO_SDA = 16; // PB09, D16 or A02 on Feather

// Flash Storage
const uint8_t FLASH_CLK = 3; // PA09, not on Feather
const uint8_t FLASH_MOSI = 4; // PA08, not on Feather (D4, used by SD-Card on Adalogger)
const uint8_t FLASH_MISO = 2; // PA14, not on Feather
const uint8_t FLASH_CS = 38; // PA13, not on Feather

// LIS3DH Accelerometer
const uint8_t LIS_CS = 10; // PA18, D10 on Feather
const uint8_t LIS_MISO = 12; // PA19, D12 on Feather
const uint8_t LIS_MOSI = 6; // PA20, D6 on Feather
const uint8_t LIS_SCK = 27; // PA28, not on Feather, yes PA28 is D27.
const uint8_t LIS_INT1 = 22; // PA12, D22/MISO on Feather
const uint8_t LIS_INT2 = 5; // PA15, D5 on Feather

// Battery status
const uint8_t BAT_CHARGE = 21; // PA23, D21/SCL on Feather
const uint8_t BAT_SENSE = 19; // PB02, D19/A5 on Feather

// Outer LED-Ring // these might be flipped?
const uint8_t LED_DATA = 24; // PB10, D23/MOSI on Feather
const uint8_t LED_CLOCK = 23; // PB11, D24/SCK on Feather

// Collections of Pins and other useful board-related constants
const uint8_t SW_COUNT = 4;
const uint8_t SWS[] = {SW1, SW2, SW3, SW4};
const uint8_t SW_LEDS[] = {SW1_LED, SW2_LED, SW3_LED, SW4_LED};

const uint8_t LED_COUNT = 8; // Outer LED-Ring
CRGB leds[LED_COUNT];

void setup() {
  FastLED.addLeds<APA102, LED_DATA, LED_CLOCK, BGR, DATA_RATE_KHZ(200)>(leds, LED_COUNT);
  digitalWrite(SW1_LED, LOW); // this is on by default, we don't want it on.
}

uint8_t rainbow_offset = 0;
uint8_t rainbow_segment = 255 / LED_COUNT;
void loop() {
  // rainbow circle
  for (uint8_t i = 0; i < LED_COUNT; i++) {
    leds[i] = CHSV(rainbow_offset + i * rainbow_segment, 255, 255);
  }
  FastLED.show();
  rainbow_offset++;
  delay(30);
}
