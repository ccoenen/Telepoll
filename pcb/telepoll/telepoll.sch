EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "Telepoll"
Date "2020-08-23"
Rev "3"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L telepoll-rescue:APA102-LED D1
U 1 1 5DB10CB8
P 6600 3100
F 0 "D1" H 6600 3581 50  0000 C CNN
F 1 "APA102" H 6600 3490 50  0000 C CNN
F 2 "LED_SMD:LED_RGB_5050-6" H 6650 2800 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 6700 2725 50  0001 L TNN
	1    6600 3100
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:APA102-LED D2
U 1 1 5DB123AB
P 7200 3100
F 0 "D2" H 7200 3581 50  0000 C CNN
F 1 "APA102" H 7200 3490 50  0000 C CNN
F 2 "LED_SMD:LED_RGB_5050-6" H 7250 2800 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 7300 2725 50  0001 L TNN
	1    7200 3100
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:APA102-LED D3
U 1 1 5DB135AD
P 7800 3100
F 0 "D3" H 7800 3581 50  0000 C CNN
F 1 "APA102" H 7800 3490 50  0000 C CNN
F 2 "LED_SMD:LED_RGB_5050-6" H 7850 2800 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 7900 2725 50  0001 L TNN
	1    7800 3100
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:APA102-LED D4
U 1 1 5DB15F5F
P 8400 3100
F 0 "D4" H 8400 3581 50  0000 C CNN
F 1 "APA102" H 8400 3490 50  0000 C CNN
F 2 "LED_SMD:LED_RGB_5050-6" H 8450 2800 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 8500 2725 50  0001 L TNN
	1    8400 3100
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:APA102-LED D5
U 1 1 5DB1693F
P 9000 3100
F 0 "D5" H 9000 3581 50  0000 C CNN
F 1 "APA102" H 9000 3490 50  0000 C CNN
F 2 "LED_SMD:LED_RGB_5050-6" H 9050 2800 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 9100 2725 50  0001 L TNN
	1    9000 3100
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:APA102-LED D6
U 1 1 5DB1694F
P 9600 3100
F 0 "D6" H 9600 3581 50  0000 C CNN
F 1 "APA102" H 9600 3490 50  0000 C CNN
F 2 "LED_SMD:LED_RGB_5050-6" H 9650 2800 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 9700 2725 50  0001 L TNN
	1    9600 3100
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:APA102-LED D8
U 1 1 5DB1695F
P 10800 3100
F 0 "D8" H 10800 3581 50  0000 C CNN
F 1 "APA102" H 10800 3490 50  0000 C CNN
F 2 "LED_SMD:LED_RGB_5050-6" H 10850 2800 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 10900 2725 50  0001 L TNN
	1    10800 3100
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:APA102-LED D7
U 1 1 5DB1696F
P 10200 3100
F 0 "D7" H 10200 3581 50  0000 C CNN
F 1 "APA102" H 10200 3490 50  0000 C CNN
F 2 "LED_SMD:LED_RGB_5050-6" H 10250 2800 50  0001 L TNN
F 3 "http://www.led-color.com/upload/201506/APA102%20LED.pdf" H 10300 2725 50  0001 L TNN
	1    10200 3100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 2450 6600 2800
Wire Wire Line
	7200 2800 7200 2450
Connection ~ 7200 2450
Wire Wire Line
	7200 2450 6600 2450
Wire Wire Line
	7800 2800 7800 2450
Connection ~ 7800 2450
Wire Wire Line
	7800 2450 7200 2450
Wire Wire Line
	8400 2800 8400 2450
Connection ~ 8400 2450
Wire Wire Line
	8400 2450 7800 2450
Wire Wire Line
	9000 2800 9000 2450
Connection ~ 9000 2450
Wire Wire Line
	9600 2800 9600 2450
Connection ~ 9600 2450
Wire Wire Line
	9600 2450 9000 2450
Wire Wire Line
	10200 2800 10200 2450
Connection ~ 10200 2450
Wire Wire Line
	10200 2450 9600 2450
Wire Wire Line
	10800 2800 10800 2450
Wire Wire Line
	10800 2450 10200 2450
$Comp
L telepoll-rescue:GND-power #PWR0103
U 1 1 5DB5999A
P 6400 3500
F 0 "#PWR0103" H 6400 3250 50  0001 C CNN
F 1 "GND" H 6405 3327 50  0000 C CNN
F 2 "" H 6400 3500 50  0001 C CNN
F 3 "" H 6400 3500 50  0001 C CNN
	1    6400 3500
	1    0    0    -1  
$EndComp
Wire Wire Line
	6400 3500 6600 3500
Wire Wire Line
	10800 3500 10800 3400
Wire Wire Line
	10200 3400 10200 3500
Connection ~ 10200 3500
Wire Wire Line
	10200 3500 10800 3500
Wire Wire Line
	9600 3400 9600 3500
Connection ~ 9600 3500
Wire Wire Line
	9600 3500 10200 3500
Wire Wire Line
	9000 3400 9000 3500
Connection ~ 9000 3500
Wire Wire Line
	9000 3500 9600 3500
Wire Wire Line
	8400 3400 8400 3500
Connection ~ 8400 3500
Wire Wire Line
	8400 3500 9000 3500
Wire Wire Line
	7800 3400 7800 3500
Connection ~ 7800 3500
Wire Wire Line
	7800 3500 8400 3500
Wire Wire Line
	7200 3400 7200 3500
Connection ~ 7200 3500
Wire Wire Line
	7200 3500 7800 3500
Wire Wire Line
	6600 3400 6600 3500
Connection ~ 6600 3500
Wire Wire Line
	6600 3500 7200 3500
Wire Wire Line
	9700 5300 9500 5300
Wire Wire Line
	9700 4200 9500 4200
Wire Wire Line
	9700 4750 9500 4750
Wire Wire Line
	9700 5850 9500 5850
$Comp
L telepoll-rescue:SW_Push-Switch SW1
U 1 1 5DB0DFD8
P 9900 4200
F 0 "SW1" H 9900 4485 50  0000 C CNN
F 1 "SW_Push" H 9900 4394 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 9900 4400 50  0001 C CNN
F 3 "~" H 9900 4400 50  0001 C CNN
	1    9900 4200
	-1   0    0    -1  
$EndComp
$Comp
L telepoll-rescue:SW_Push-Switch SW4
U 1 1 5DB0F86B
P 9900 5850
F 0 "SW4" H 9900 6135 50  0000 C CNN
F 1 "SW_Push" H 9900 6044 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 9900 6050 50  0001 C CNN
F 3 "~" H 9900 6050 50  0001 C CNN
	1    9900 5850
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:SW_Push-Switch SW3
U 1 1 5DB0F25F
P 9900 5300
F 0 "SW3" H 9900 5585 50  0000 C CNN
F 1 "SW_Push" H 9900 5494 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 9900 5500 50  0001 C CNN
F 3 "~" H 9900 5500 50  0001 C CNN
	1    9900 5300
	-1   0    0    -1  
$EndComp
$Comp
L telepoll-rescue:SW_Push-Switch SW2
U 1 1 5DB0E741
P 9900 4750
F 0 "SW2" H 9900 5035 50  0000 C CNN
F 1 "SW_Push" H 9900 4944 50  0000 C CNN
F 2 "Button_Switch_Keyboard:SW_Cherry_MX_1.00u_PCB" H 9900 4950 50  0001 C CNN
F 3 "~" H 9900 4950 50  0001 C CNN
	1    9900 4750
	1    0    0    -1  
$EndComp
Connection ~ 9500 4750
Connection ~ 9500 5300
Wire Wire Line
	5050 6750 5100 6750
Wire Wire Line
	5150 6850 5100 6850
$Comp
L telepoll-rescue:+BATT-power #PWR0105
U 1 1 5DB675BF
P 5100 6750
F 0 "#PWR0105" H 5100 6600 50  0001 C CNN
F 1 "+BATT" H 5115 6923 50  0000 C CNN
F 2 "" H 5100 6750 50  0001 C CNN
F 3 "" H 5100 6750 50  0001 C CNN
	1    5100 6750
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:+3V3-power #PWR0107
U 1 1 5DB32CB3
P 2900 2900
F 0 "#PWR0107" H 2900 2750 50  0001 C CNN
F 1 "+3V3" H 2915 3073 50  0000 C CNN
F 2 "" H 2900 2900 50  0001 C CNN
F 3 "" H 2900 2900 50  0001 C CNN
	1    2900 2900
	1    0    0    -1  
$EndComp
Text GLabel 5350 1600 2    50   Input ~ 0
USB-D+
Text GLabel 4050 5250 2    50   BiDi ~ 0
USB-D-
Text GLabel 4050 5350 2    50   BiDi ~ 0
USB-D+
$Comp
L telepoll-rescue:GND-power #PWR0109
U 1 1 5DBC0573
P 750 3000
F 0 "#PWR0109" H 750 2750 50  0001 C CNN
F 1 "GND" H 755 2827 50  0000 C CNN
F 2 "" H 750 3000 50  0001 C CNN
F 3 "" H 750 3000 50  0001 C CNN
	1    750  3000
	1    0    0    -1  
$EndComp
NoConn ~ 11100 3000
NoConn ~ 11100 3100
$Comp
L telepoll-rescue:Conn_01x02_Male-Connector J2
U 1 1 5DBB21EC
P 5350 6850
F 0 "J2" H 5412 6894 50  0000 L CNN
F 1 "Conn_01x02_Male" H 5300 6350 50  0001 L CNN
F 2 "Connector_JST:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" H 5350 6850 50  0001 C CNN
F 3 "~" H 5350 6850 50  0001 C CNN
	1    5350 6850
	-1   0    0    1   
$EndComp
$Comp
L telepoll-rescue:GND-power #PWR0102
U 1 1 5DBB7B96
P 9500 6300
F 0 "#PWR0102" H 9500 6050 50  0001 C CNN
F 1 "GND" H 9650 6250 50  0000 C CNN
F 2 "" H 9500 6300 50  0001 C CNN
F 3 "" H 9500 6300 50  0001 C CNN
	1    9500 6300
	1    0    0    -1  
$EndComp
Connection ~ 9500 5850
$Comp
L telepoll-rescue:LED-Device D9
U 1 1 5DBBF1D7
P 8800 4200
F 0 "D9" H 8793 3945 50  0000 C CNN
F 1 "LED" H 8793 4036 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 8800 4200 50  0001 C CNN
F 3 "~" H 8800 4200 50  0001 C CNN
	1    8800 4200
	-1   0    0    -1  
$EndComp
$Comp
L telepoll-rescue:LED-Device D10
U 1 1 5DBDD718
P 8800 4750
F 0 "D10" H 8793 4495 50  0000 C CNN
F 1 "LED" H 8793 4586 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 8800 4750 50  0001 C CNN
F 3 "~" H 8800 4750 50  0001 C CNN
	1    8800 4750
	-1   0    0    -1  
$EndComp
$Comp
L telepoll-rescue:LED-Device D11
U 1 1 5DBDD9DC
P 8800 5300
F 0 "D11" H 8793 5045 50  0000 C CNN
F 1 "LED" H 8793 5136 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 8800 5300 50  0001 C CNN
F 3 "~" H 8800 5300 50  0001 C CNN
	1    8800 5300
	-1   0    0    -1  
$EndComp
$Comp
L telepoll-rescue:LED-Device D12
U 1 1 5DBDDDE5
P 8800 5850
F 0 "D12" H 8793 5595 50  0000 C CNN
F 1 "LED" H 8793 5686 50  0000 C CNN
F 2 "LED_THT:LED_D3.0mm" H 8800 5850 50  0001 C CNN
F 3 "~" H 8800 5850 50  0001 C CNN
	1    8800 5850
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9250 4200 9500 4200
Connection ~ 9500 4200
Wire Wire Line
	9250 4750 9500 4750
Wire Wire Line
	9250 5300 9500 5300
Wire Wire Line
	9250 5850 9500 5850
Text GLabel 10750 4200 2    50   Output ~ 0
SW1
Text GLabel 10750 4750 2    50   Output ~ 0
SW2
Text GLabel 10750 5300 2    50   Output ~ 0
SW3
Text GLabel 10750 5850 2    50   Output ~ 0
SW4
Text GLabel 8650 4200 0    50   Input ~ 0
SW1_LED
Text GLabel 8650 4750 0    50   Input ~ 0
SW2_LED
Text GLabel 8650 5300 0    50   Input ~ 0
SW3_LED
Text GLabel 8650 5850 0    50   Input ~ 0
SW4_LED
$Comp
L telepoll-rescue:+3V3-power #PWR0104
U 1 1 5DC373C3
P 3300 800
F 0 "#PWR0104" H 3300 650 50  0001 C CNN
F 1 "+3V3" H 3315 973 50  0000 C CNN
F 2 "" H 3300 800 50  0001 C CNN
F 3 "" H 3300 800 50  0001 C CNN
	1    3300 800 
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:GND-power #PWR0111
U 1 1 5DC3FFA6
P 3300 2000
F 0 "#PWR0111" H 3300 1750 50  0001 C CNN
F 1 "GND" H 3305 1827 50  0000 C CNN
F 2 "" H 3300 2000 50  0001 C CNN
F 3 "" H 3300 2000 50  0001 C CNN
	1    3300 2000
	1    0    0    -1  
$EndComp
Text GLabel 2800 1100 0    50   Input ~ 0
COPI
Text GLabel 2800 1200 0    50   Input ~ 0
CIPO
Text GLabel 2800 1300 0    50   Input ~ 0
SCK
Text GLabel 2800 1700 0    50   Input ~ 0
NRF-INT
Text GLabel 4050 5750 2    50   Input ~ 0
NRF-INT
Text GLabel 1350 6050 0    50   Output ~ 0
COPI
Text GLabel 1350 6150 0    50   Output ~ 0
SCK
$Comp
L telepoll-rescue:NRF24L01_Breakout-RF U2
U 1 1 5DB613D4
P 3300 1400
F 0 "U2" H 2750 800 50  0000 C BNN
F 1 "NRF24L01_Breakout" H 2750 750 50  0000 C TNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x04_P2.54mm_Vertical" H 3450 2000 50  0001 L CIN
F 3 "http://www.nordicsemi.com/eng/content/download/2730/34105/file/nRF24L01_Product_Specification_v2_0.pdf" H 3300 1300 50  0001 C CNN
	1    3300 1400
	1    0    0    -1  
$EndComp
Text GLabel 2800 1400 0    50   Input ~ 0
NRF-CSN
Text GLabel 2800 1600 0    50   Input ~ 0
NRF-CE
Text GLabel 1350 5350 0    50   Output ~ 0
NRF-CSN
Text GLabel 1350 5250 0    50   Output ~ 0
NRF-CE
Text Notes 500  7750 0    50   ~ 0
Based heavily on Adafruit's M0 Adalogger\nhttps://learn.adafruit.com/adafruit-feather-m0-adalogger\nLots of Cues from the Numberwang Badge\nhttps://github.com/PTS93/Numberwang-Badge
$Comp
L telepoll-rescue:C-Device C1
U 1 1 5DD9286A
P 3050 3950
F 0 "C1" V 2798 3950 50  0000 C CNN
F 1 "1uF" V 2889 3950 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3088 3800 50  0001 C CNN
F 3 "~" H 3050 3950 50  0001 C CNN
F 4 "C28323" H 3050 3950 50  0001 C CNN "LCSC"
F 5 "GRM21BR71C105KA01L" H 3050 3950 50  0001 C CNN "SeeedStudio"
	1    3050 3950
	0    1    1    0   
$EndComp
$Comp
L telepoll-rescue:GND-power #PWR0113
U 1 1 5DD9AE48
P 3300 4000
F 0 "#PWR0113" H 3300 3750 50  0001 C CNN
F 1 "GND" H 3305 3827 50  0000 C CNN
F 2 "" H 3300 4000 50  0001 C CNN
F 3 "" H 3300 4000 50  0001 C CNN
	1    3300 4000
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:R-Device R1
U 1 1 5DDB23CC
P 9100 4200
F 0 "R1" V 8893 4200 50  0000 C CNN
F 1 "R150" V 8984 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9030 4200 50  0001 C CNN
F 3 "~" H 9100 4200 50  0001 C CNN
F 4 "C17471" H 9100 4200 50  0001 C CNN "LCSC"
F 5 "RC0805FR-07150RL" H 9100 4200 50  0001 C CNN "SeeedStudio"
	1    9100 4200
	0    -1   -1   0   
$EndComp
$Comp
L telepoll-rescue:R-Device R2
U 1 1 5DDB2A4E
P 9100 4750
F 0 "R2" V 8893 4750 50  0000 C CNN
F 1 "R150" V 8984 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9030 4750 50  0001 C CNN
F 3 "~" H 9100 4750 50  0001 C CNN
F 4 "C17471" H 9100 4750 50  0001 C CNN "LCSC"
F 5 "RC0805FR-07150RL" H 9100 4750 50  0001 C CNN "SeeedStudio"
	1    9100 4750
	0    -1   -1   0   
$EndComp
$Comp
L telepoll-rescue:R-Device R3
U 1 1 5DDB2CF8
P 9100 5300
F 0 "R3" V 8893 5300 50  0000 C CNN
F 1 "R150" V 8984 5300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9030 5300 50  0001 C CNN
F 3 "~" H 9100 5300 50  0001 C CNN
F 4 "C17471" H 9100 5300 50  0001 C CNN "LCSC"
F 5 "RC0805FR-07150RL" H 9100 5300 50  0001 C CNN "SeeedStudio"
	1    9100 5300
	0    -1   -1   0   
$EndComp
$Comp
L telepoll-rescue:R-Device R4
U 1 1 5DDB3023
P 9100 5850
F 0 "R4" V 8893 5850 50  0000 C CNN
F 1 "R150" V 8984 5850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9030 5850 50  0001 C CNN
F 3 "~" H 9100 5850 50  0001 C CNN
F 4 "C17471" H 9100 5850 50  0001 C CNN "LCSC"
F 5 "RC0805FR-07150RL" H 9100 5850 50  0001 C CNN "SeeedStudio"
	1    9100 5850
	0    -1   -1   0   
$EndComp
$Comp
L telepoll-rescue:C-Device C5
U 1 1 5DB6363B
P 10550 6000
F 0 "C5" V 10298 6000 50  0000 C CNN
F 1 "100nF" V 10389 6000 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10588 5850 50  0001 C CNN
F 3 "~" H 10550 6000 50  0001 C CNN
F 4 "C49678" H 10550 6000 50  0001 C CNN "LCSC"
F 5 "0805B104K500NT" H 10550 6000 50  0001 C CNN "SeeedStudio"
	1    10550 6000
	-1   0    0    -1  
$EndComp
$Comp
L telepoll-rescue:C-Device C4
U 1 1 5DB6EC92
P 10550 5450
F 0 "C4" V 10298 5450 50  0000 C CNN
F 1 "100nF" V 10389 5450 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10588 5300 50  0001 C CNN
F 3 "~" H 10550 5450 50  0001 C CNN
F 4 "C49678" H 10550 5450 50  0001 C CNN "LCSC"
F 5 "0805B104K500NT" H 10550 5450 50  0001 C CNN "SeeedStudio"
	1    10550 5450
	-1   0    0    -1  
$EndComp
$Comp
L telepoll-rescue:C-Device C3
U 1 1 5DB6EFA0
P 10550 4900
F 0 "C3" V 10298 4900 50  0000 C CNN
F 1 "100nF" V 10389 4900 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10588 4750 50  0001 C CNN
F 3 "~" H 10550 4900 50  0001 C CNN
F 4 "C49678" H 10550 4900 50  0001 C CNN "LCSC"
F 5 "0805B104K500NT" H 10550 4900 50  0001 C CNN "SeeedStudio"
	1    10550 4900
	-1   0    0    -1  
$EndComp
$Comp
L telepoll-rescue:C-Device C2
U 1 1 5DB6F2AC
P 10550 4350
F 0 "C2" V 10298 4350 50  0000 C CNN
F 1 "100nF" V 10389 4350 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 10588 4200 50  0001 C CNN
F 3 "~" H 10550 4350 50  0001 C CNN
F 4 "C49678" H 10550 4350 50  0001 C CNN "LCSC"
F 5 "0805B104K500NT" H 10550 4350 50  0001 C CNN "SeeedStudio"
	1    10550 4350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	10750 4200 10550 4200
Wire Wire Line
	10750 4750 10550 4750
Wire Wire Line
	10750 5300 10550 5300
Wire Wire Line
	10750 5850 10550 5850
Connection ~ 10550 4200
Wire Wire Line
	10550 4200 10400 4200
Connection ~ 10550 4750
Wire Wire Line
	10550 4750 10400 4750
Connection ~ 10550 5300
Wire Wire Line
	10550 5300 10400 5300
Connection ~ 10550 5850
Wire Wire Line
	10550 5850 10400 5850
$Comp
L telepoll-rescue:C-Device C7
U 1 1 5DBF84F3
P 3050 3550
F 0 "C7" V 2798 3550 50  0000 C CNN
F 1 "100nF" V 2889 3550 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3088 3400 50  0001 C CNN
F 3 "~" H 3050 3550 50  0001 C CNN
F 4 "C49678" H 3050 3550 50  0001 C CNN "LCSC"
F 5 "0805B104K500NT" H 3050 3550 50  0001 C CNN "SeeedStudio"
	1    3050 3550
	0    1    1    0   
$EndComp
Wire Wire Line
	2650 4250 2650 3950
Text Notes 2700 3800 1    20   ~ 0
intentionally\nnot connected to 3V3!
Text GLabel 4050 5850 2    50   Input ~ 0
SWD-CLK
Text GLabel 4050 5950 2    50   BiDi ~ 0
SWD-IO
Text GLabel 6600 7250 0    50   Input ~ 0
RESET
Text GLabel 6600 7450 0    50   Input ~ 0
SWD-CLK
Text GLabel 6600 7350 0    50   Input ~ 0
SWD-IO
$Comp
L telepoll-rescue:+3V3-power #PWR0117
U 1 1 5DCC477C
P 6050 7150
F 0 "#PWR0117" H 6050 7000 50  0001 C CNN
F 1 "+3V3" H 6200 7200 50  0000 C CNN
F 2 "" H 6050 7150 50  0001 C CNN
F 3 "" H 6050 7150 50  0001 C CNN
	1    6050 7150
	1    0    0    -1  
$EndComp
Wire Wire Line
	6050 7150 6600 7150
Text Notes 5950 6950 0    50   ~ 0
SWD Connector
Text Notes 11100 6450 2    50   ~ 0
Cherry-Switches with Low-Pass\nMCU needs to provide Pull-Up
Wire Notes Line
	11150 3750 5550 3750
Text Notes 5600 2300 0    50   ~ 0
LED-Ring
Wire Notes Line
	5550 2200 11150 2200
Wire Notes Line
	8000 6000 5550 6000
Wire Notes Line
	5550 2200 5550 3750
Wire Notes Line
	2250 2300 2250 550 
Wire Notes Line
	3750 550  3750 2300
Text Notes 2300 650  0    50   ~ 0
Radio
NoConn ~ 2650 3850
Text GLabel 4050 5650 2    50   Input ~ 0
SW1
Text GLabel 1350 4750 0    50   Input ~ 0
SW2
Text GLabel 4050 4650 2    50   Input ~ 0
SW3
Text GLabel 1350 4950 0    50   Input ~ 0
SW4
Text GLabel 4050 5050 2    50   Output ~ 0
SW1_LED
Text GLabel 1350 4650 0    50   Output ~ 0
SW2_LED
Text GLabel 4050 4750 2    50   Output ~ 0
SW3_LED
Text GLabel 1350 5050 0    50   Output ~ 0
SW4_LED
Text GLabel 4050 4850 2    50   Output ~ 0
LED-DATA-3V3
Text GLabel 4050 4950 2    50   Output ~ 0
LED-CLOCK-3V3
$Comp
L telepoll-rescue:GND-power #PWR0116
U 1 1 5DE79B21
P 6050 7550
F 0 "#PWR0116" H 6050 7300 50  0001 C CNN
F 1 "GND" H 6200 7500 50  0000 C CNN
F 2 "" H 6050 7550 50  0001 C CNN
F 3 "" H 6050 7550 50  0001 C CNN
	1    6050 7550
	1    0    0    -1  
$EndComp
Text GLabel 1150 2650 0    50   Input ~ 0
BAT-SENSE
Wire Wire Line
	9500 4200 9500 4500
Wire Wire Line
	9500 4750 9500 5050
$Comp
L telepoll-rescue:R-Device R5
U 1 1 5DB8D0D9
P 10250 4200
F 0 "R5" V 10043 4200 50  0000 C CNN
F 1 "R1K" V 10134 4200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 10180 4200 50  0001 C CNN
F 3 "~" H 10250 4200 50  0001 C CNN
F 4 "C17513" H 10250 4200 50  0001 C CNN "LCSC"
F 5 "RC0805JR-071KL" H 10250 4200 50  0001 C CNN "SeeedStudio"
	1    10250 4200
	0    -1   1    0   
$EndComp
$Comp
L telepoll-rescue:R-Device R6
U 1 1 5DB8D0DF
P 10250 4750
F 0 "R6" V 10043 4750 50  0000 C CNN
F 1 "R1K" V 10134 4750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 10180 4750 50  0001 C CNN
F 3 "~" H 10250 4750 50  0001 C CNN
F 4 "C17513" H 10250 4750 50  0001 C CNN "LCSC"
F 5 "RC0805JR-071KL" H 10250 4750 50  0001 C CNN "SeeedStudio"
	1    10250 4750
	0    -1   1    0   
$EndComp
$Comp
L telepoll-rescue:R-Device R7
U 1 1 5DB8D0E5
P 10250 5300
F 0 "R7" V 10043 5300 50  0000 C CNN
F 1 "R1K" V 10134 5300 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 10180 5300 50  0001 C CNN
F 3 "~" H 10250 5300 50  0001 C CNN
F 4 "C17513" H 10250 5300 50  0001 C CNN "LCSC"
F 5 "RC0805JR-071KL" H 10250 5300 50  0001 C CNN "SeeedStudio"
	1    10250 5300
	0    -1   1    0   
$EndComp
$Comp
L telepoll-rescue:R-Device R8
U 1 1 5DB8D0EB
P 10250 5850
F 0 "R8" V 10043 5850 50  0000 C CNN
F 1 "R1K" V 10134 5850 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 10180 5850 50  0001 C CNN
F 3 "~" H 10250 5850 50  0001 C CNN
F 4 "C17513" H 10250 5850 50  0001 C CNN "LCSC"
F 5 "RC0805JR-071KL" H 10250 5850 50  0001 C CNN "SeeedStudio"
	1    10250 5850
	0    -1   1    0   
$EndComp
Connection ~ 9500 5600
Wire Wire Line
	9500 5600 9500 5850
Wire Wire Line
	10550 5600 9500 5600
Wire Wire Line
	9500 5300 9500 5600
Connection ~ 9500 5050
Wire Wire Line
	9500 5050 9500 5300
Wire Wire Line
	10550 5050 9500 5050
Wire Wire Line
	10550 4500 9500 4500
Connection ~ 9500 4500
Wire Wire Line
	9500 4500 9500 4750
Wire Notes Line
	8100 3850 11150 3850
Wire Notes Line
	8100 6500 11150 6500
Text Notes 8200 3950 0    50   ~ 0
LEDs + Switches
$Comp
L Regulator_Linear:AP2112K-3.3 U5
U 1 1 5DBFA06E
P 10450 1250
F 0 "U5" H 10450 1592 50  0000 C CNN
F 1 "AP2112K-3.3" H 10450 1501 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 10450 1575 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/AP2112.pdf" H 10450 1350 50  0001 C CNN
F 4 "C51118" H 10450 1250 50  0001 C CNN "LCSC"
F 5 "AP2112K-3.3TRG1" H 10450 1250 50  0001 C CNN "SeeedStudio"
	1    10450 1250
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:C-Device C12
U 1 1 5DC4D633
P 10950 1500
F 0 "C12" V 10698 1500 50  0000 C CNN
F 1 "10uF" V 10789 1500 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.39x1.80mm_HandSolder" H 10988 1350 50  0001 C CNN
F 3 "~" H 10950 1500 50  0001 C CNN
F 4 "C13585" H 10950 1500 50  0001 C CNN "LCSC"
F 5 "CL31F106ZOHNNNE" H 10950 1500 50  0001 C CNN "SeeedStudio"
	1    10950 1500
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:C-Device C11
U 1 1 5DC4DCB2
P 9750 1500
F 0 "C11" V 9498 1500 50  0000 C CNN
F 1 "10uF" V 9589 1500 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.39x1.80mm_HandSolder" H 9788 1350 50  0001 C CNN
F 3 "~" H 9750 1500 50  0001 C CNN
F 4 "C13585" H 9750 1500 50  0001 C CNN "LCSC"
F 5 "CL31F106ZOHNNNE" H 9750 1500 50  0001 C CNN "SeeedStudio"
	1    9750 1500
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:GND-power #PWR0110
U 1 1 5DC5B3F4
P 6900 1700
F 0 "#PWR0110" H 6900 1450 50  0001 C CNN
F 1 "GND" H 6905 1527 50  0000 C CNN
F 2 "" H 6900 1700 50  0001 C CNN
F 3 "" H 6900 1700 50  0001 C CNN
	1    6900 1700
	1    0    0    -1  
$EndComp
Text Notes 6150 1950 1    30   ~ 0
RProg = ~~5K = ~~200mA charging current
$Comp
L telepoll-rescue:GND-power #PWR0118
U 1 1 5DC6297F
P 6450 1800
F 0 "#PWR0118" H 6450 1550 50  0001 C CNN
F 1 "GND" H 6455 1627 50  0000 C CNN
F 2 "" H 6450 1800 50  0001 C CNN
F 3 "" H 6450 1800 50  0001 C CNN
	1    6450 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	6450 1750 6450 1800
Wire Wire Line
	7300 1250 7400 1250
Text GLabel 7200 2000 0    50   Output ~ 0
BAT-CHARGE
Text GLabel 4050 5150 2    50   Input ~ 0
BAT-CHARGE
$Comp
L telepoll-rescue:GND-power #PWR0119
U 1 1 5DCAFE4F
P 10450 1700
F 0 "#PWR0119" H 10450 1450 50  0001 C CNN
F 1 "GND" H 10455 1527 50  0000 C CNN
F 2 "" H 10450 1700 50  0001 C CNN
F 3 "" H 10450 1700 50  0001 C CNN
	1    10450 1700
	1    0    0    -1  
$EndComp
Wire Wire Line
	10450 1550 10450 1650
$Comp
L telepoll-rescue:+3V3-power #PWR0120
U 1 1 5DCB8DBF
P 10950 900
F 0 "#PWR0120" H 10950 750 50  0001 C CNN
F 1 "+3V3" H 10965 1073 50  0000 C CNN
F 2 "" H 10950 900 50  0001 C CNN
F 3 "" H 10950 900 50  0001 C CNN
	1    10950 900 
	1    0    0    -1  
$EndComp
Wire Wire Line
	10750 1150 10950 1150
Wire Wire Line
	10950 1150 10950 900 
Wire Wire Line
	10950 1150 10950 1350
Connection ~ 10950 1150
Wire Wire Line
	10950 1650 10450 1650
Connection ~ 10450 1650
Wire Wire Line
	10450 1650 10450 1700
Wire Wire Line
	9750 1650 10450 1650
$Comp
L telepoll-rescue:+BATT-power #PWR0121
U 1 1 5DD23593
P 7400 1200
F 0 "#PWR0121" H 7400 1050 50  0001 C CNN
F 1 "+BATT" H 7415 1373 50  0000 C CNN
F 2 "" H 7400 1200 50  0001 C CNN
F 3 "" H 7400 1200 50  0001 C CNN
	1    7400 1200
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 1250 7500 1250
Connection ~ 7400 1250
$Comp
L power:VBUS #PWR0122
U 1 1 5DE2D3A1
P 5450 6750
F 0 "#PWR0122" H 5450 6600 50  0001 C CNN
F 1 "VBUS" H 5465 6923 50  0000 C CNN
F 2 "" H 5450 6750 50  0001 C CNN
F 3 "" H 5450 6750 50  0001 C CNN
	1    5450 6750
	1    0    0    -1  
$EndComp
$Comp
L power:VBUS #PWR0123
U 1 1 5DE2D72E
P 7200 850
F 0 "#PWR0123" H 7200 700 50  0001 C CNN
F 1 "VBUS" H 7215 1023 50  0000 C CNN
F 2 "" H 7200 850 50  0001 C CNN
F 3 "" H 7200 850 50  0001 C CNN
	1    7200 850 
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:R-Device R12
U 1 1 5DE74E47
P 9950 1250
F 0 "R12" V 9743 1250 50  0000 C CNN
F 1 "R100K" V 9834 1250 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 9880 1250 50  0001 C CNN
F 3 "~" H 9950 1250 50  0001 C CNN
F 4 "C17407" H 9950 1250 50  0001 C CNN "LCSC"
F 5 "RC0805FR-07100KL" H 9950 1250 50  0001 C CNN "SeeedStudio"
	1    9950 1250
	0    1    -1   0   
$EndComp
Wire Wire Line
	10150 1250 10100 1250
Wire Wire Line
	9800 1250 9750 1250
Text Notes 9350 1800 1    50   ~ 0
Note: VDC might be anything\nbetween 3,5 and 5,5 Volts!
$Comp
L telepoll-rescue:R-Device R10
U 1 1 5DF1B2C7
P 1300 3000
F 0 "R10" V 1093 3000 50  0000 C CNN
F 1 "R100K" V 1184 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 1230 3000 50  0001 C CNN
F 3 "~" H 1300 3000 50  0001 C CNN
F 4 "C17407" H 1300 3000 50  0001 C CNN "LCSC"
F 5 "RC0805FR-07100KL" H 1300 3000 50  0001 C CNN "SeeedStudio"
	1    1300 3000
	0    1    1    0   
$EndComp
$Comp
L telepoll-rescue:R-Device R11
U 1 1 5DF1B778
P 1000 3000
F 0 "R11" V 793 3000 50  0000 C CNN
F 1 "R100K" V 884 3000 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 930 3000 50  0001 C CNN
F 3 "~" H 1000 3000 50  0001 C CNN
F 4 "C17407" H 1000 3000 50  0001 C CNN "LCSC"
F 5 "RC0805FR-07100KL" H 1000 3000 50  0001 C CNN "SeeedStudio"
	1    1000 3000
	0    1    1    0   
$EndComp
Wire Wire Line
	1150 2650 1150 3000
Connection ~ 1150 3000
$Comp
L telepoll-rescue:+BATT-power #PWR0125
U 1 1 5DF3607F
P 1550 3000
F 0 "#PWR0125" H 1550 2850 50  0001 C CNN
F 1 "+BATT" H 1565 3173 50  0000 C CNN
F 2 "" H 1550 3000 50  0001 C CNN
F 3 "" H 1550 3000 50  0001 C CNN
	1    1550 3000
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:GND-power #PWR0126
U 1 1 5DF36E00
P 5450 6850
F 0 "#PWR0126" H 5450 6600 50  0001 C CNN
F 1 "GND" H 5455 6677 50  0000 C CNN
F 2 "" H 5450 6850 50  0001 C CNN
F 3 "" H 5450 6850 50  0001 C CNN
	1    5450 6850
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 1200 7400 1250
Text GLabel 4050 6150 2    50   Input ~ 0
RESET
Text GLabel 1350 4850 0    50   Input ~ 0
BAT-SENSE
$Comp
L telepoll-rescue:GND-power #PWR0129
U 1 1 5DDB7582
P 1350 2050
F 0 "#PWR0129" H 1350 1800 50  0001 C CNN
F 1 "GND" H 1355 1877 50  0000 C CNN
F 2 "" H 1350 2050 50  0001 C CNN
F 3 "" H 1350 2050 50  0001 C CNN
	1    1350 2050
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:+3V3-power #PWR0130
U 1 1 5DDB79A2
P 1350 800
F 0 "#PWR0130" H 1350 650 50  0001 C CNN
F 1 "+3V3" H 1365 973 50  0000 C CNN
F 2 "" H 1350 800 50  0001 C CNN
F 3 "" H 1350 800 50  0001 C CNN
	1    1350 800 
	1    0    0    -1  
$EndComp
Text GLabel 950  1400 0    50   Input ~ 0
COPI
Text GLabel 950  1500 0    50   Input ~ 0
CIPO
Text GLabel 950  1600 0    50   Input ~ 0
SCK
Text GLabel 1850 1400 2    50   Input ~ 0
LIS-INT1
Text GLabel 950  1300 0    50   Input ~ 0
LIS-CSN
Text GLabel 1350 5650 0    50   Output ~ 0
LIS-CSN
Text Notes 600  2500 0    50   ~ 0
Battery Readout
Wire Notes Line
	2150 2300 2150 550 
Wire Notes Line
	2150 550  550  550 
Wire Notes Line
	550  550  550  2300
Wire Notes Line
	550  2300 2150 2300
Text Notes 600  650  0    50   ~ 0
IMU
Wire Notes Line
	11150 2200 11150 3750
Wire Notes Line
	5550 6000 5550 4400
Wire Notes Line
	11150 1900 11150 550 
Wire Notes Line
	11150 550  9400 550 
Wire Notes Line
	9400 550  9400 1900
Wire Notes Line
	9400 1900 11150 1900
Text Notes 9450 650  0    50   ~ 0
LDO
Text Notes 6150 650  0    50   ~ 0
Dealing with the battery
Wire Notes Line
	1900 3300 550  3300
Wire Notes Line
	550  3300 550  2400
Wire Notes Line
	550  2400 1900 2400
Wire Notes Line
	1900 2400 1900 3300
Wire Notes Line
	6000 550  6000 1900
Wire Notes Line
	3850 550  6000 550 
Text Notes 4850 6450 0    50   ~ 0
Power Connectors
Text GLabel 6250 3000 0    50   Input ~ 0
LED-DATA-5V
Text GLabel 6250 3100 0    50   Input ~ 0
LED-CLOCK-5V
Text GLabel 6300 4750 0    50   Input ~ 0
LED-DATA-3V3
Text GLabel 6300 5750 0    50   Input ~ 0
LED-CLOCK-3V3
$Comp
L telepoll-rescue:+3V3-power #PWR0114
U 1 1 5E0257A2
P 5700 5100
F 0 "#PWR0114" H 5700 4950 50  0001 C CNN
F 1 "+3V3" H 5715 5273 50  0000 C CNN
F 2 "" H 5700 5100 50  0001 C CNN
F 3 "" H 5700 5100 50  0001 C CNN
	1    5700 5100
	1    0    0    -1  
$EndComp
Text Notes 5600 4600 0    50   ~ 0
3,3V to VDC\nlogic level shifter
Text GLabel 7150 4750 2    50   Output ~ 0
LED-DATA-5V
Text GLabel 7150 5750 2    50   Output ~ 0
LED-CLOCK-5V
Wire Wire Line
	5700 5100 5700 5250
Wire Wire Line
	5700 5250 6700 5250
Wire Wire Line
	6700 5250 6700 5050
Connection ~ 6700 5250
Wire Wire Line
	6700 5250 6700 5450
$Comp
L telepoll-rescue:+3V3-power #PWR0133
U 1 1 5E0B4BA3
P 7550 5100
F 0 "#PWR0133" H 7550 4950 50  0001 C CNN
F 1 "+3V3" H 7565 5273 50  0000 C CNN
F 2 "" H 7550 5100 50  0001 C CNN
F 3 "" H 7550 5100 50  0001 C CNN
	1    7550 5100
	1    0    0    -1  
$EndComp
Wire Wire Line
	6300 5750 6400 5750
Wire Wire Line
	6300 4750 6400 4750
Wire Wire Line
	6400 5100 6400 4750
Connection ~ 6400 4750
Wire Wire Line
	6400 4750 6500 4750
Connection ~ 6400 5750
Wire Wire Line
	6400 5750 6500 5750
$Comp
L telepoll-rescue:BSS138-Transistor_FET Q2
U 1 1 5DC6588A
P 6700 5650
F 0 "Q2" V 6951 5650 50  0000 C CNN
F 1 "BSS138" V 6950 5950 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6900 5575 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/BS/BSS138.pdf" H 6700 5650 50  0001 L CNN
F 4 "C82045" H 6700 5650 50  0001 C CNN "LCSC"
F 5 "BSS138W-7-F" H 6700 5650 50  0001 C CNN "SeeedStudio"
	1    6700 5650
	0    1    1    0   
$EndComp
$Comp
L telepoll-rescue:BSS138-Transistor_FET Q1
U 1 1 5DC4AD19
P 6700 4850
F 0 "Q1" V 6951 4850 50  0000 C CNN
F 1 "BSS138" V 6950 5150 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 6900 4775 50  0001 L CIN
F 3 "https://www.fairchildsemi.com/datasheets/BS/BSS138.pdf" H 6700 4850 50  0001 L CNN
F 4 "C82045" H 6700 4850 50  0001 C CNN "LCSC"
F 5 "BSS138W-7-F" H 6700 4850 50  0001 C CNN "SeeedStudio"
	1    6700 4850
	0    1    -1   0   
$EndComp
Wire Notes Line
	5550 4400 8000 4400
Wire Notes Line
	8000 4400 8000 6000
Wire Wire Line
	6900 4750 7000 4750
Wire Wire Line
	7000 5200 7000 4750
Connection ~ 7000 4750
Wire Wire Line
	7000 4750 7150 4750
$Comp
L telepoll-rescue:Conn_01x05_Female-Connector J3
U 1 1 5DCBE59C
P 6800 7350
F 0 "J3" H 6850 6900 50  0000 R CNN
F 1 "Conn_01x05_Female" H 6850 7000 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x05_P2.54mm_Vertical" H 6800 7350 50  0001 C CNN
F 3 "~" H 6800 7350 50  0001 C CNN
	1    6800 7350
	1    0    0    1   
$EndComp
Wire Notes Line
	5900 6850 6900 6850
Wire Notes Line
	6900 6850 6900 7700
Wire Notes Line
	6900 7700 5900 7700
Wire Notes Line
	5900 7700 5900 6850
Wire Wire Line
	1350 2000 1350 2050
Wire Wire Line
	1350 800  1350 850 
Connection ~ 1350 850 
Wire Wire Line
	1350 850  1450 850 
$Comp
L telepoll-rescue:Conn_01x02_Male-Connector J6
U 1 1 5E627DDE
P 5750 6850
F 0 "J6" H 5812 6894 50  0000 L CNN
F 1 "Conn_01x02_Male" H 5700 6350 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x02_P2.54mm_Vertical" H 5750 6850 50  0001 C CNN
F 3 "~" H 5750 6850 50  0001 C CNN
	1    5750 6850
	-1   0    0    1   
$EndComp
Wire Wire Line
	5550 6750 5450 6750
NoConn ~ 4900 2000
NoConn ~ 4900 2100
$Comp
L power:VBUS #PWR0134
U 1 1 5DC1AF73
P 5200 900
F 0 "#PWR0134" H 5200 750 50  0001 C CNN
F 1 "VBUS" H 5215 1073 50  0000 C CNN
F 2 "" H 5200 900 50  0001 C CNN
F 3 "" H 5200 900 50  0001 C CNN
	1    5200 900 
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:GND-power #PWR0135
U 1 1 5DC1B41B
P 4650 2550
F 0 "#PWR0135" H 4650 2300 50  0001 C CNN
F 1 "GND" H 4655 2377 50  0000 C CNN
F 2 "" H 4650 2550 50  0001 C CNN
F 3 "" H 4650 2550 50  0001 C CNN
	1    4650 2550
	1    0    0    -1  
$EndComp
Text GLabel 5350 1400 2    50   Input ~ 0
USB-D-
Wire Wire Line
	5550 6850 5450 6850
Text Notes 5100 4800 2    50   ~ 0
default LED-Pin
Wire Notes Line
	6000 1900 5450 1900
Wire Notes Line
	5450 2800 3850 2800
$Comp
L telepoll-rescue:R-Device R14
U 1 1 5DC24AAE
P 8450 1750
F 0 "R14" V 8243 1750 50  0000 C CNN
F 1 "R100K" V 8334 1750 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 8380 1750 50  0001 C CNN
F 3 "~" H 8450 1750 50  0001 C CNN
F 4 "C17407" H 8450 1750 50  0001 C CNN "LCSC"
F 5 "RC0805FR-07100KL" H 8450 1750 50  0001 C CNN "SeeedStudio"
	1    8450 1750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8950 1600 9000 1600
Connection ~ 8450 1600
Wire Wire Line
	8450 1600 8550 1600
Wire Wire Line
	8150 1900 8450 1900
Connection ~ 8450 1900
Wire Wire Line
	8450 1900 8750 1900
$Comp
L Switch:SW_SPDT SW5
U 1 1 5DDB74E3
P 7950 1800
F 0 "SW5" H 7900 1700 50  0000 C CNN
F 1 "SW_SPDT" H 7950 1600 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPDT_CK-JS102011SAQN" H 7950 1800 50  0001 C CNN
F 3 "~" H 7950 1800 50  0001 C CNN
F 4 "C221660" H 7950 1800 50  0001 C CNN "LCSC"
	1    7950 1800
	1    0    0    1   
$EndComp
$Comp
L telepoll-rescue:GND-power #PWR0138
U 1 1 5DDB7F19
P 7700 1800
F 0 "#PWR0138" H 7700 1550 50  0001 C CNN
F 1 "GND" H 7705 1627 50  0000 C CNN
F 2 "" H 7700 1800 50  0001 C CNN
F 3 "" H 7700 1800 50  0001 C CNN
	1    7700 1800
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:GND-power #PWR0106
U 1 1 5DE4B2FA
P 5100 6850
F 0 "#PWR0106" H 5100 6600 50  0001 C CNN
F 1 "GND" H 5105 6677 50  0000 C CNN
F 2 "" H 5100 6850 50  0001 C CNN
F 3 "" H 5100 6850 50  0001 C CNN
	1    5100 6850
	1    0    0    -1  
$EndComp
Text GLabel 1350 5150 0    50   Input ~ 0
LIS-INT1
$Comp
L Connector:TestPoint TPD1
U 1 1 5DC29DF2
P 6300 2900
F 0 "TPD1" H 6358 3018 50  0000 L CNN
F 1 "TestPoint" H 6358 2927 50  0000 L CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 6500 2900 50  0001 C CNN
F 3 "~" H 6500 2900 50  0001 C CNN
	1    6300 2900
	-1   0    0    -1  
$EndComp
$Comp
L Connector:TestPoint TPC1
U 1 1 5DC36038
P 6300 3200
F 0 "TPC1" H 6243 3226 50  0000 R CNN
F 1 "TestPoint" H 6243 3317 50  0000 R CNN
F 2 "TestPoint:TestPoint_Pad_D1.5mm" H 6500 3200 50  0001 C CNN
F 3 "~" H 6500 3200 50  0001 C CNN
	1    6300 3200
	1    0    0    1   
$EndComp
Wire Wire Line
	6250 3100 6300 3100
Wire Wire Line
	6300 3100 6300 3200
Connection ~ 6300 3100
Wire Wire Line
	6250 3000 6300 3000
Wire Wire Line
	6300 3000 6300 2900
Connection ~ 6300 3000
$Comp
L telepoll-rescue:SW_Push-Switch SW6
U 1 1 5DD78321
P 1250 3800
F 0 "SW6" H 1250 4085 50  0000 C CNN
F 1 "SW_Push" H 1250 3994 50  0000 C CNN
F 2 "Button_Switch_SMD:SW_SPST_CK_KXT3" H 1250 4000 50  0001 C CNN
F 3 "~" H 1250 4000 50  0001 C CNN
F 4 "C221820" H 1250 3800 50  0001 C CNN "LCSC"
	1    1250 3800
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:GND-power #PWR0139
U 1 1 5DD78600
P 1450 3800
F 0 "#PWR0139" H 1450 3550 50  0001 C CNN
F 1 "GND" H 1455 3627 50  0000 C CNN
F 2 "" H 1450 3800 50  0001 C CNN
F 3 "" H 1450 3800 50  0001 C CNN
	1    1450 3800
	-1   0    0    -1  
$EndComp
Text GLabel 1050 3800 0    50   Input ~ 0
RESET
Wire Notes Line
	550  3400 1550 3400
Wire Notes Line
	1550 3400 1550 4050
Wire Notes Line
	1550 4050 550  4050
Wire Notes Line
	550  4050 550  3400
Text Notes 600  3500 0    50   ~ 0
Reset-button
Text Notes 8150 6450 0    50   ~ 0
well below 7mA Maximum\neven with 100 Ohm resistor
$Comp
L telepoll-rescue:R-Device R16
U 1 1 5DEC04E2
P 5050 1100
F 0 "R16" V 4950 1100 50  0000 C CNN
F 1 "R5K1" V 5050 1100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4980 1100 50  0001 C CNN
F 3 "~" H 5050 1100 50  0001 C CNN
F 4 "C27834" H 5050 1100 50  0001 C CNN "LCSC"
F 5 "RC0805FR-075K1L" H 5050 1100 50  0001 C CNN "SeeedStudio"
	1    5050 1100
	0    1    1    0   
$EndComp
$Comp
L telepoll-rescue:R-Device R17
U 1 1 5DEC08DE
P 5050 1200
F 0 "R17" V 5150 1200 50  0000 C CNN
F 1 "R5K1" V 5050 1200 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4980 1200 50  0001 C CNN
F 3 "~" H 5050 1200 50  0001 C CNN
F 4 "C27834" H 5050 1200 50  0001 C CNN "LCSC"
F 5 "RC0805FR-075K1L" H 5050 1200 50  0001 C CNN "SeeedStudio"
	1    5050 1200
	0    1    1    0   
$EndComp
$Comp
L telepoll-rescue:GND-power #PWR01
U 1 1 5DF26045
P 5800 1650
F 0 "#PWR01" H 5800 1400 50  0001 C CNN
F 1 "GND" H 5805 1477 50  0000 C CNN
F 2 "" H 5800 1650 50  0001 C CNN
F 3 "" H 5800 1650 50  0001 C CNN
	1    5800 1650
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 1200 5800 1200
Wire Wire Line
	5200 1100 5800 1100
Wire Wire Line
	5800 1100 5800 1200
Connection ~ 5800 1200
Wire Wire Line
	4900 900  5200 900 
$Comp
L telepoll-rescue:R-Device R15
U 1 1 5DF6A8DA
P 4150 2500
F 0 "R15" V 4250 2500 50  0000 C CNN
F 1 "R1M" V 4150 2500 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 4080 2500 50  0001 C CNN
F 3 "~" H 4150 2500 50  0001 C CNN
F 4 "C269728" H 4150 2500 50  0001 C CNN "LCSC"
F 5 "RC0805FR-071ML" H 4150 2500 50  0001 C CNN "SeeedStudio"
	1    4150 2500
	0    -1   -1   0   
$EndComp
$Comp
L telepoll-rescue:C-Device C9
U 1 1 5DF925E6
P 4150 2650
F 0 "C9" V 4200 2500 50  0000 C CNN
F 1 "100nF" V 4200 2850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4188 2500 50  0001 C CNN
F 3 "~" H 4150 2650 50  0001 C CNN
F 4 "C49678" H 4150 2650 50  0001 C CNN "LCSC"
F 5 "0805B104K500NT" H 4150 2650 50  0001 C CNN "SeeedStudio"
	1    4150 2650
	0    1    1    0   
$EndComp
Wire Wire Line
	4000 2400 4000 2500
Wire Wire Line
	4300 2400 4300 2450
$Comp
L Device:D_Zener D15
U 1 1 5DED7094
P 5050 1800
F 0 "D15" V 5000 1550 50  0000 L CNN
F 1 "Z3.6V" V 5100 1500 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-323" H 5050 1800 50  0001 C CNN
F 3 "~" H 5050 1800 50  0001 C CNN
F 4 "C209558" H 5050 1800 50  0001 C CNN "LCSC"
	1    5050 1800
	0    1    1    0   
$EndComp
$Comp
L telepoll-rescue:GND-power #PWR0115
U 1 1 5DED7521
P 5200 1950
F 0 "#PWR0115" H 5200 1700 50  0001 C CNN
F 1 "GND" H 5205 1777 50  0000 C CNN
F 2 "" H 5200 1950 50  0001 C CNN
F 3 "" H 5200 1950 50  0001 C CNN
	1    5200 1950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4900 1400 4900 1500
Wire Wire Line
	5050 1400 5050 1650
Connection ~ 5050 1400
Wire Wire Line
	5050 1400 5350 1400
Wire Wire Line
	4900 1600 4900 1700
Text Notes 3900 650  0    50   ~ 0
USB-C
Wire Wire Line
	5800 1200 5800 1650
$Comp
L Device:D_Zener D14
U 1 1 5DEC6C55
P 5200 1800
F 0 "D14" V 5154 1879 50  0000 L CNN
F 1 "Z3.6V" V 5245 1879 50  0000 L CNN
F 2 "Diode_SMD:D_SOD-323" H 5200 1800 50  0001 C CNN
F 3 "~" H 5200 1800 50  0001 C CNN
F 4 "C209558" H 5200 1800 50  0001 C CNN "LCSC"
	1    5200 1800
	0    1    1    0   
$EndComp
Wire Wire Line
	5050 1950 5200 1950
Connection ~ 5200 1950
Wire Wire Line
	5200 1650 5200 1600
Connection ~ 5200 1600
Wire Wire Line
	5200 1600 5350 1600
Wire Wire Line
	4900 1400 5050 1400
Wire Wire Line
	4900 1600 5200 1600
$Comp
L Connector:USB_C_Receptacle_USB2.0 J5
U 1 1 5E67975C
P 4300 1500
F 0 "J5" H 4407 2367 50  0000 C CNN
F 1 "USB_C_Receptacle_USB2.0" H 4407 2276 50  0000 C CNN
F 2 "telepoll:USB_C_Receptacle_HRO_TYPE-C-31-M-12" H 4450 1500 50  0001 C CNN
F 3 "https://www.usb.org/sites/default/files/documents/usb_type-c.zip" H 4450 1500 50  0001 C CNN
F 4 "C165948" H 4300 1500 50  0001 C CNN "LCSC"
F 5 "TYPE-C-31-M-12" H 4300 1500 50  0001 C CNN "SeeedStudio"
	1    4300 1500
	1    0    0    -1  
$EndComp
Wire Notes Line
	5800 6350 5800 7100
Wire Notes Line
	5800 7100 4800 7100
Wire Notes Line
	4800 7100 4800 6350
Wire Notes Line
	4800 6350 5800 6350
Wire Wire Line
	9750 1050 9750 1150
Wire Wire Line
	9750 1250 9750 1350
Connection ~ 9750 1250
Wire Wire Line
	10150 1150 9750 1150
Connection ~ 9750 1150
Wire Wire Line
	9750 1150 9750 1250
Wire Wire Line
	6050 7550 6600 7550
Wire Wire Line
	3300 4000 3300 3950
Wire Wire Line
	3300 3150 3200 3150
Wire Wire Line
	3200 3550 3300 3550
Connection ~ 3300 3550
Wire Wire Line
	3300 3550 3300 3150
Wire Wire Line
	3200 3950 3300 3950
Connection ~ 3300 3950
Wire Wire Line
	3300 3950 3300 3550
Wire Wire Line
	2900 2900 2900 3150
Wire Wire Line
	2900 3950 2650 3950
Wire Wire Line
	2450 4250 2450 3150
Wire Wire Line
	2450 3150 2900 3150
Connection ~ 2900 3150
Wire Wire Line
	2850 4250 2850 3550
Wire Wire Line
	2850 3550 2900 3550
Wire Wire Line
	2650 3850 2650 3950
Connection ~ 2650 3950
Wire Wire Line
	2900 3150 2900 3550
Connection ~ 2900 3550
$Comp
L telepoll-rescue:GND-power #PWR0108
U 1 1 5E35438D
P 2550 6550
F 0 "#PWR0108" H 2550 6300 50  0001 C CNN
F 1 "GND" H 2555 6377 50  0000 C CNN
F 2 "" H 2550 6550 50  0001 C CNN
F 3 "" H 2550 6550 50  0001 C CNN
	1    2550 6550
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:GND-power #PWR0127
U 1 1 5E3548A3
P 2750 6550
F 0 "#PWR0127" H 2750 6300 50  0001 C CNN
F 1 "GND" H 2755 6377 50  0000 C CNN
F 2 "" H 2750 6550 50  0001 C CNN
F 3 "" H 2750 6550 50  0001 C CNN
	1    2750 6550
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:ATSAMD21E18A-AUT U1
U 1 1 5E0E03BA
P 2650 5350
F 0 "U1" H 2650 4061 50  0000 C CNN
F 1 "ATSAMD21E18A-AUT" H 2650 3900 50  0000 C CNN
F 2 "Package_QFP:TQFP-32_7x7mm_P0.8mm" H 1700 4300 50  0001 C CNN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/SAMD21-Family-DataSheet-DS40001882D.pdf" H 2650 5550 50  0001 C CNN
	1    2650 5350
	1    0    0    -1  
$EndComp
Text Notes 1100 5900 2    50   ~ 0
default UART (Sercom0)
Text GLabel 1350 5750 0    50   Input ~ 0
CIPO
Text Notes 400  8100 0    50   ~ 0
see how to assign sercom1 (LED-SPI, SPI_PAD_2_SCK_3, no RX) and sercom2-alt (SPI, PAD2_SCK3, RX1):\nhttps://learn.adafruit.com/using-atsamd21-sercom-to-add-more-spi-i2c-serial-ports/creating-a-new-spi
Text Notes 1100 6150 2    50   ~ 0
new SPI (Sercom2-alt)
Text Notes 5450 4900 2    50   ~ 0
new SPI (Sercom1)
Wire Notes Line
	2250 550  3750 550 
Wire Notes Line
	3750 2300 2250 2300
Text GLabel 1350 5950 0    50   Input ~ 0
RX
Text GLabel 1350 5850 0    50   Output ~ 0
TX
Wire Wire Line
	1450 3000 1550 3000
Wire Wire Line
	750  3000 850  3000
Text Notes 5950 6200 0    50   ~ 0
Serial Connector
Wire Notes Line
	5900 6100 6900 6100
Wire Notes Line
	6900 6750 5900 6750
Text GLabel 6500 6450 0    50   Input ~ 0
TX
Text GLabel 6500 6650 0    50   Input ~ 0
RX
Wire Wire Line
	6200 6550 6500 6550
$Comp
L Connector:Conn_01x03_Female J1
U 1 1 5E59742F
P 6700 6550
F 0 "J1" H 6750 6200 50  0000 R CNN
F 1 "Conn_01x03_Female" H 6750 6300 50  0000 R CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 6700 6550 50  0001 C CNN
F 3 "~" H 6700 6550 50  0001 C CNN
	1    6700 6550
	1    0    0    1   
$EndComp
$Comp
L telepoll-rescue:GND-power #PWR0128
U 1 1 5E597429
P 6200 6550
F 0 "#PWR0128" H 6200 6300 50  0001 C CNN
F 1 "GND" H 6350 6500 50  0000 C CNN
F 2 "" H 6200 6550 50  0001 C CNN
F 3 "" H 6200 6550 50  0001 C CNN
	1    6200 6550
	-1   0    0    -1  
$EndComp
Wire Notes Line
	5900 6100 5900 6750
Wire Notes Line
	6900 6100 6900 6750
$Comp
L Device:Q_PMOS_GSD Q3
U 1 1 5E0F8F80
P 7700 1150
F 0 "Q3" V 7300 1100 50  0000 L CNN
F 1 "DMG3415U-7" V 7400 900 50  0000 L CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7900 1250 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/Diodes-Incorporated-DMG3415U-7_C96616.pdf" H 7700 1150 50  0001 C CNN
F 4 "C96616" H 7700 1150 50  0001 C CNN "LCSC"
	1    7700 1150
	0    -1   1    0   
$EndComp
$Comp
L Device:Q_PMOS_GSD Q4
U 1 1 5E105E8A
P 8750 1700
F 0 "Q4" V 9000 1700 50  0000 C CNN
F 1 "DMG3415U-7" V 8450 1700 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 8950 1800 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/Diodes-Incorporated-DMG3415U-7_C96616.pdf" H 8750 1700 50  0001 C CNN
F 4 "C96616" H 8750 1700 50  0001 C CNN "LCSC"
	1    8750 1700
	0    1    -1   0   
$EndComp
Wire Wire Line
	7700 950  7200 950 
Connection ~ 7200 950 
Wire Wire Line
	7200 850  7200 950 
$Comp
L telepoll-rescue:C-Device C6
U 1 1 5DC21494
P 3050 3150
F 0 "C6" V 2798 3150 50  0000 C CNN
F 1 "100nF" V 2889 3150 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3088 3000 50  0001 C CNN
F 3 "~" H 3050 3150 50  0001 C CNN
F 4 "C49678" H 3050 3150 50  0001 C CNN "LCSC"
F 5 "0805B104K500NT" H 3050 3150 50  0001 C CNN "SeeedStudio"
	1    3050 3150
	0    1    1    0   
$EndComp
$Comp
L telepoll-rescue:C-Device C8
U 1 1 5E4B3A1D
P 1000 850
F 0 "C8" V 1150 850 50  0000 C CNN
F 1 "100nF" V 850 850 50  0000 C CNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 1038 700 50  0001 C CNN
F 3 "~" H 1000 850 50  0001 C CNN
F 4 "C49678" H 1000 850 50  0001 C CNN "LCSC"
F 5 "0805B104K500NT" H 1000 850 50  0001 C CNN "SeeedStudio"
	1    1000 850 
	0    1    1    0   
$EndComp
$Comp
L telepoll-rescue:GND-power #PWR0131
U 1 1 5E4D313E
P 750 850
F 0 "#PWR0131" H 750 600 50  0001 C CNN
F 1 "GND" H 755 677 50  0000 C CNN
F 2 "" H 750 850 50  0001 C CNN
F 3 "" H 750 850 50  0001 C CNN
	1    750  850 
	1    0    0    -1  
$EndComp
Wire Wire Line
	750  850  850  850 
Connection ~ 5100 6750
Wire Wire Line
	5100 6750 5150 6750
Connection ~ 5100 6850
Wire Wire Line
	5100 6850 5050 6850
$Comp
L telepoll-rescue:Conn_01x02_Male-Connector J4
U 1 1 5E0D84B2
P 4850 6750
F 0 "J4" H 4900 6600 50  0000 L CNN
F 1 "Conn_01x02_Male" H 4800 6250 50  0001 L CNN
F 2 "Connector_JST:JST_PH_B2B-PH-K_1x02_P2.00mm_Vertical" H 4850 6750 50  0001 C CNN
F 3 "~" H 4850 6750 50  0001 C CNN
	1    4850 6750
	1    0    0    -1  
$EndComp
Wire Wire Line
	7850 5150 7850 5200
Wire Wire Line
	6400 5750 6400 5400
Wire Wire Line
	7000 5300 7100 5300
$Comp
L telepoll-rescue:R_Pack04-Device RN1
U 1 1 5DC46E35
P 7300 5200
F 0 "RN1" V 7500 5200 50  0000 C CNN
F 1 "10K" V 7600 5200 50  0000 C CNN
F 2 "Resistor_SMD:R_Array_Convex_4x0603" V 7575 5200 50  0001 C CNN
F 3 "https://datasheet.lcsc.com/szlcsc/1904151512_Ever-Ohms-Tech-CRA064RJ10K0E04Z_C183863.pdf" H 7300 5200 50  0001 C CNN
F 4 "C25804" H 7300 5200 50  0001 C CNN "LCSC"
	1    7300 5200
	0    1    -1   0   
$EndComp
Wire Wire Line
	7500 5400 7550 5400
Wire Wire Line
	7550 5100 7550 5400
Wire Wire Line
	7500 5300 7850 5300
Wire Wire Line
	7500 5200 7850 5200
Connection ~ 7850 5200
Wire Wire Line
	7850 5200 7850 5300
Wire Wire Line
	7000 5200 7100 5200
Wire Wire Line
	7500 5100 7550 5100
Connection ~ 7550 5100
Wire Wire Line
	6400 5100 7100 5100
Wire Wire Line
	6900 5750 7000 5750
Wire Wire Line
	6400 5400 7100 5400
Wire Wire Line
	7000 5300 7000 5750
Connection ~ 7000 5750
Wire Wire Line
	7000 5750 7150 5750
$Comp
L Mechanical:Fiducial FID1
U 1 1 5E12DF33
P 4700 7300
F 0 "FID1" H 4785 7346 50  0000 L CNN
F 1 "Fiducial" H 4785 7255 50  0000 L CNN
F 2 "Fiducial:Fiducial_0.75mm_Dia_1.5mm_Outer" H 4700 7300 50  0001 C CNN
F 3 "~" H 4700 7300 50  0001 C CNN
	1    4700 7300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID3
U 1 1 5E12E599
P 5300 7300
F 0 "FID3" H 5385 7346 50  0000 L CNN
F 1 "Fiducial" H 5385 7255 50  0000 L CNN
F 2 "Fiducial:Fiducial_0.75mm_Dia_1.5mm_Outer" H 5300 7300 50  0001 C CNN
F 3 "~" H 5300 7300 50  0001 C CNN
	1    5300 7300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID2
U 1 1 5E1438E0
P 4700 7550
F 0 "FID2" H 4785 7596 50  0000 L CNN
F 1 "Fiducial" H 4785 7505 50  0000 L CNN
F 2 "Fiducial:Fiducial_0.75mm_Dia_1.5mm_Outer" H 4700 7550 50  0001 C CNN
F 3 "~" H 4700 7550 50  0001 C CNN
	1    4700 7550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:Fiducial FID4
U 1 1 5E143A49
P 5300 7550
F 0 "FID4" H 5385 7596 50  0000 L CNN
F 1 "Fiducial" H 5385 7505 50  0000 L CNN
F 2 "Fiducial:Fiducial_0.75mm_Dia_1.5mm_Outer" H 5300 7550 50  0001 C CNN
F 3 "~" H 5300 7550 50  0001 C CNN
	1    5300 7550
	1    0    0    -1  
$EndComp
$Comp
L power:VDC #PWR0101
U 1 1 5E166757
P 9000 1050
F 0 "#PWR0101" H 9000 950 50  0001 C CNN
F 1 "VDC" H 9000 1325 50  0000 C CNN
F 2 "" H 9000 1050 50  0001 C CNN
F 3 "" H 9000 1050 50  0001 C CNN
	1    9000 1050
	1    0    0    -1  
$EndComp
$Comp
L power:VDC #PWR0112
U 1 1 5E1674D5
P 9750 1050
F 0 "#PWR0112" H 9750 950 50  0001 C CNN
F 1 "VDC" H 9750 1325 50  0000 C CNN
F 2 "" H 9750 1050 50  0001 C CNN
F 3 "" H 9750 1050 50  0001 C CNN
	1    9750 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8400 2450 8700 2450
$Comp
L power:VDC #PWR0124
U 1 1 5E1737B3
P 8700 2450
F 0 "#PWR0124" H 8700 2350 50  0001 C CNN
F 1 "VDC" H 8850 2600 50  0000 C CNN
F 2 "" H 8700 2450 50  0001 C CNN
F 3 "" H 8700 2450 50  0001 C CNN
	1    8700 2450
	1    0    0    -1  
$EndComp
Connection ~ 8700 2450
Wire Wire Line
	8700 2450 9000 2450
$Comp
L power:VDC #PWR0132
U 1 1 5E174126
P 7850 5150
F 0 "#PWR0132" H 7850 5050 50  0001 C CNN
F 1 "VDC" H 7850 5425 50  0000 C CNN
F 2 "" H 7850 5150 50  0001 C CNN
F 3 "" H 7850 5150 50  0001 C CNN
	1    7850 5150
	1    0    0    -1  
$EndComp
$Comp
L Graphic:Logo_Open_Hardware_Small #LOGO1
U 1 1 5E22E099
P 4200 7450
F 0 "#LOGO1" H 4200 7725 50  0001 C CNN
F 1 "Logo_Open_Hardware_Small" H 4200 7225 50  0001 C CNN
F 2 "" H 4200 7450 50  0001 C CNN
F 3 "~" H 4200 7450 50  0001 C CNN
	1    4200 7450
	1    0    0    -1  
$EndComp
Wire Notes Line
	5450 1900 5450 2800
Wire Notes Line
	3850 550  3850 2800
Connection ~ 4000 2500
Wire Wire Line
	4000 2500 4000 2650
Connection ~ 4300 2500
Wire Wire Line
	4300 2500 4300 2450
Connection ~ 4900 1400
Connection ~ 4900 1600
Wire Wire Line
	4650 2450 4300 2450
Connection ~ 4300 2450
Wire Wire Line
	4650 2450 4650 2550
Wire Wire Line
	4300 2500 4300 2650
$Comp
L telepoll-rescue:C-Device C13
U 1 1 5E169609
P 7400 1400
F 0 "C13" V 7148 1400 50  0000 C CNN
F 1 "10uF" V 7239 1400 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.39x1.80mm_HandSolder" H 7438 1250 50  0001 C CNN
F 3 "~" H 7400 1400 50  0001 C CNN
F 4 "C13585" H 7400 1400 50  0001 C CNN "LCSC"
F 5 "CL31F106ZOHNNNE" H 7400 1400 50  0001 C CNN "SeeedStudio"
	1    7400 1400
	-1   0    0    -1  
$EndComp
$Comp
L telepoll-rescue:R-Device R13
U 1 1 5E176529
P 7950 1100
F 0 "R13" V 7743 1100 50  0000 C CNN
F 1 "R100K" V 7834 1100 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 7880 1100 50  0001 C CNN
F 3 "~" H 7950 1100 50  0001 C CNN
F 4 "C17407" H 7950 1100 50  0001 C CNN "LCSC"
F 5 "RC0805FR-07100KL" H 7950 1100 50  0001 C CNN "SeeedStudio"
	1    7950 1100
	-1   0    0    -1  
$EndComp
Wire Wire Line
	7900 1250 7950 1250
$Comp
L Diode:MBR0520 D13
U 1 1 5E0E0D1D
P 8250 1100
F 0 "D13" H 8250 900 50  0000 C CNN
F 1 "SK22WA" H 8250 1000 50  0000 C CNN
F 2 "Diode_SMD:D_SOD-123" H 8250 925 50  0001 C CNN
F 3 "http://www.mccsemi.com/up_pdf/MBR0520~MBR0580(SOD123).pdf" H 8250 1100 50  0001 C CNN
F 4 "C183473" H 8250 1100 50  0001 C CNN "LCSC"
	1    8250 1100
	0    -1   -1   0   
$EndComp
Connection ~ 7950 950 
Wire Wire Line
	7950 950  8250 950 
Connection ~ 7950 1250
Wire Wire Line
	7950 1250 8250 1250
Wire Wire Line
	7700 950  7950 950 
$Comp
L telepoll-rescue:GND-power #PWR0136
U 1 1 5E1FB49E
P 7400 1800
F 0 "#PWR0136" H 7400 1550 50  0001 C CNN
F 1 "GND" H 7405 1627 50  0000 C CNN
F 2 "" H 7400 1800 50  0001 C CNN
F 3 "" H 7400 1800 50  0001 C CNN
	1    7400 1800
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:GND-power #PWR0137
U 1 1 5E1FB8A8
P 6450 950
F 0 "#PWR0137" H 6450 700 50  0001 C CNN
F 1 "GND" H 6455 777 50  0000 C CNN
F 2 "" H 6450 950 50  0001 C CNN
F 3 "" H 6450 950 50  0001 C CNN
	1    6450 950 
	1    0    0    -1  
$EndComp
Wire Wire Line
	7400 1800 7400 1550
Wire Wire Line
	6450 950  6550 950 
Wire Wire Line
	7700 1800 7750 1800
Wire Wire Line
	9500 5850 9500 6150
Wire Wire Line
	10550 6150 9500 6150
Connection ~ 9500 6150
Wire Wire Line
	9500 6150 9500 6300
Wire Notes Line
	11150 3850 11150 6500
Wire Notes Line
	8100 3850 8100 6500
Wire Wire Line
	7300 1450 7300 2000
Wire Wire Line
	7300 2000 7200 2000
Wire Wire Line
	9000 1050 9000 1600
Wire Wire Line
	8250 1250 8250 1600
Connection ~ 8250 1250
Wire Wire Line
	6450 1450 6500 1450
NoConn ~ 8150 1700
Connection ~ 7700 950 
Wire Wire Line
	8250 1600 8450 1600
Wire Notes Line
	8550 550  8550 1450
Wire Notes Line
	8550 1450 7750 1450
Wire Notes Line
	7750 1450 7750 1650
Wire Notes Line
	7750 1650 7550 1650
Wire Notes Line
	7550 1650 7550 2100
Wire Notes Line
	6100 550  6100 2100
Wire Notes Line
	6100 550  9150 550 
Wire Notes Line
	9150 550  9150 2100
Wire Notes Line
	6100 2100 9150 2100
Text Notes 8600 650  0    50   ~ 0
Main Power
Wire Wire Line
	6900 1650 6900 1700
$Comp
L telepoll-rescue:C-Device C10
U 1 1 5E16ACC8
P 6700 950
F 0 "C10" V 6448 950 50  0000 C CNN
F 1 "10uF" V 6539 950 50  0000 C CNN
F 2 "Capacitor_SMD:C_1206_3216Metric_Pad1.39x1.80mm_HandSolder" H 6738 800 50  0001 C CNN
F 3 "~" H 6700 950 50  0001 C CNN
F 4 "C13585" H 6700 950 50  0001 C CNN "LCSC"
F 5 "CL31F106ZOHNNNE" H 6700 950 50  0001 C CNN "SeeedStudio"
	1    6700 950 
	0    1    1    0   
$EndComp
Wire Wire Line
	6900 1050 6900 950 
Connection ~ 6900 950 
Wire Wire Line
	6900 950  6850 950 
Wire Wire Line
	7200 950  6900 950 
$Comp
L Battery_Management:MCP73831-2-OT U4
U 1 1 5DC02141
P 6900 1350
F 0 "U4" H 6300 1850 50  0000 C CNN
F 1 "MCP73831-2-OT" H 6300 1950 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5_HandSoldering" H 6950 1100 50  0001 L CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/20001984g.pdf" H 6750 1300 50  0001 C CNN
F 4 "C424093" H 6900 1350 50  0001 C CNN "LCSC"
	1    6900 1350
	1    0    0    -1  
$EndComp
$Comp
L telepoll-rescue:R-Device R9
U 1 1 5DC5B6F4
P 6450 1600
F 0 "R9" V 6243 1600 50  0000 C CNN
F 1 "R5K1" V 6334 1600 50  0000 C CNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" V 6380 1600 50  0001 C CNN
F 3 "~" H 6450 1600 50  0001 C CNN
F 4 "C27834" H 6450 1600 50  0001 C CNN "LCSC"
F 5 "RC0805FR-075K1L" H 6450 1600 50  0001 C CNN "SeeedStudio"
	1    6450 1600
	1    0    0    -1  
$EndComp
$Comp
L Sensor_Motion:LIS2DE12 U3
U 1 1 5F43C37E
P 1350 1500
F 0 "U3" H 1750 2100 50  0000 C CNN
F 1 "LIS2DE12" H 1750 2000 50  0000 C CNN
F 2 "Package_LGA:LGA-12_2x2mm_P0.5mm" H 1500 2050 50  0001 L CNN
F 3 "https://www.st.com/resource/en/datasheet/lis2DE12.pdf" H 1000 1500 50  0001 C CNN
	1    1350 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1250 2000 1350 2000
Connection ~ 1350 2000
Wire Wire Line
	1450 2000 1350 2000
Wire Wire Line
	1350 850  1350 1000
Wire Wire Line
	1450 850  1450 1000
NoConn ~ 1850 1500
NoConn ~ 1850 1800
Wire Wire Line
	1150 850  1350 850 
$EndSCHEMATC
