Telepoll
========

A tiny button box that can be operated in a number of ways:
- Regular Keyboard input
- Wireless communication (nRF24L01)
- On-Device Games

Based extensively on work by Adafruit and Timon
- https://github.com/adafruit/Adafruit-Feather-M0-Adalogger-PCB
- https://github.com/adafruit/Adafruit-Feather-M0-Express-PCB
- https://github.com/PTS93/Numberwang-Badge


Technological Specs
-------------------

- 4 Keys (Cherry MX Style)
- SAMD21 Microcontroller
- USB-C
- Battery charging
- RGB-LED Ring
- Additional LED per Key
- Accelerometer (LIS3DH)
- Flash Storage


For new boards
--------------
https://circuitpython.org
Flash u2f loader via Atmel Studio to address 0x0000
Upload Circuit Python u2f to that.


Libraries
---------

https://github.com/adafruit/Adafruit_CircuitPython_Pypixelbuf
https://github.com/adafruit/Adafruit_CircuitPython_DotStar
https://github.com/adafruit/Adafruit_CircuitPython_HID
https://github.com/adafruit/Adafruit_CircuitPython_BusDevice
https://github.com/adafruit/Adafruit_CircuitPython_LIS3DH
https://github.com/2bndy5/CircuitPython_nRF24L01
