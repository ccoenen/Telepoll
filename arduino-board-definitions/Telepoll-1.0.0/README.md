# Arduino Core for Telepoll

Based on Adafruit's SAMD21 core here: 
https://github.com/adafruit/ArduinoCore-samd

Also based on MattairTech's SAMD21 work here:
https://github.com/mattairtech/ArduinoCore-samd

With the help of these tutorials and documentations:
- https://www.instructables.com/id/Arduino-IDE-Creating-Custom-Boards/
- https://www.avdweb.nl/arduino/samd21/samd21-variant
- https://github.com/mattairtech/ArduinoCore-samd/tree/master/variants/MT_D21E_revB

## License and credits

This core has been developed by Arduino LLC in collaboration with Atmel. It has been modified to fit the Telepoll board.

```
  Copyright (c) 2015 Arduino LLC.  All right reserved.

  This library is free software; you can redistribute it and/or
  modify it under the terms of the GNU Lesser General Public
  License as published by the Free Software Foundation; either
  version 2.1 of the License, or (at your option) any later version.

  This library is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
  See the GNU Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public
  License along with this library; if not, write to the Free Software
  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
```
